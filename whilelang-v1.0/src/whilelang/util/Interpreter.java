// This file is part of the WhileLang Compiler (wlc).
//
// The WhileLang Compiler is free software; you can redistribute
// it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// The WhileLang Compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with the WhileLang Compiler. If not, see
// <http://www.gnu.org/licenses/>
//
// Copyright 2013, David James Pearce.

package whilelang.util;

import static whilelang.util.SyntaxError.internalFailure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import whilelang.ast.Expr;
import whilelang.ast.Stmt;
import whilelang.ast.WhileFile;

/**
 * A simple interpreter for WhileLang programs, which executes them in their
 * Abstract Syntax Tree form directly. The interpreter is not designed to be
 * efficient in anyway, however it's purpose is to provide a reference
 * implementation for the language.
 *
 * @author David J. Pearce
 *
 */
public class Interpreter {
	private HashMap<String, WhileFile.Decl> declarations;
	private WhileFile file;

	public void run(WhileFile wf) {
		// First, initialise the map of declaration names to their bodies.
		declarations = new HashMap<String,WhileFile.Decl>();
		for(WhileFile.Decl decl : wf.declarations) {
			declarations.put(decl.name(), decl);
		}
		this.file = wf;

		// Second, pick the main method (if one exits) and execute it
		WhileFile.Decl main = declarations.get("main");
		if(main instanceof WhileFile.MethodDecl) {
			WhileFile.MethodDecl fd = (WhileFile.MethodDecl) main;
			execute(fd);
		} else {
			System.out.println("Cannot find a main() function");
		}
	}

	/**
	 * Execute a given function with the given argument values. If the number of
	 * arguments is incorrect, then an exception is thrown.
	 *
	 * @param function
	 *            Function declaration to execute.
	 * @param arguments
	 *            Array of argument values.
	 */
	private Object execute(WhileFile.MethodDecl function, Object... arguments) {

		// First, sanity check the number of arguments
		if(function.getParameters().size() != arguments.length){
			throw new RuntimeException(
					"invalid number of arguments supplied to execution of function \""
							+ function.getName() + "\"");
		}

		// Second, construct the stack frame in which this function will
		// execute.
		HashMap<String,Object> frame = new HashMap<String,Object>();
		for(int i=0;i!=arguments.length;++i) {
			WhileFile.Parameter parameter = function.getParameters().get(i);
			frame.put(parameter.getName(),arguments[i]);
		}

		// Third, execute the function body!
		return execute(function.getBody(),frame);
	}

	private Object execute(List<Stmt> block, HashMap<String,Object> frame) {
		for(int i=0;i!=block.size();i=i+1) {
			Object r = execute(block.get(i),frame);
			if(r != null) {
				return r;
			}
		}
		return null;
	}

	/**
	 * Execute a given statement in a given stack frame.
	 *
	 * @param stmt
	 *            Statement to execute.
	 * @param frame
	 *            Stack frame mapping variables to their current value.
	 * @return
	 */
	private Object execute(Stmt stmt, HashMap<String,Object> frame) {
		if(stmt instanceof Stmt.Assert) {
			return execute((Stmt.Assert) stmt,frame);
		} else if(stmt instanceof Stmt.Assign) {
			return execute((Stmt.Assign) stmt,frame);
		} else if(stmt instanceof Stmt.For) {
			return execute((Stmt.For) stmt,frame);
		} else if(stmt instanceof Stmt.ForEach) {
			return execute((Stmt.ForEach) stmt,frame);
		} else if(stmt instanceof Stmt.Try) {
			return execute((Stmt.Try) stmt,frame);
		} else if(stmt instanceof Stmt.Catch) {
			return execute((Stmt.Catch) stmt,frame);
		} else if(stmt instanceof Stmt.Throw) {
			return execute((Stmt.Throw) stmt,frame);
		} else if(stmt instanceof Stmt.While) {
			return execute((Stmt.While) stmt,frame);
		} else if(stmt instanceof Stmt.Switch) {
			return execute((Stmt.Switch) stmt,frame);
		} else if(stmt instanceof Stmt.Break) {
			return execute((Stmt.Break) stmt,frame);
		} else if(stmt instanceof Stmt.Continue) {
			return execute((Stmt.Continue) stmt,frame);
		} else if(stmt instanceof Stmt.IfElse) {
			return execute((Stmt.IfElse) stmt,frame);
		} else if(stmt instanceof Stmt.Return) {
			return execute((Stmt.Return) stmt,frame);
		} else if(stmt instanceof Stmt.VariableDeclaration) {
			return execute((Stmt.VariableDeclaration) stmt,frame);
		} else if(stmt instanceof Expr.Invoke) {
			return execute((Expr.Invoke) stmt,frame);
		} else {
			internalFailure("unknown statement encountered (" + stmt + ")", file.filename,stmt);
			return null;
		}
	}

	private Object execute(Stmt.Assert stmt, HashMap<String,Object> frame) {
		//System.out.println("inner loop : " + stmt.getExpr().toString() + frame.toString());
		
		//System.out.println("throw caught " + execute(stmt.getExpr(),frame).toString() );
		Object obj = execute(stmt.getExpr(),frame);
		if(obj.toString().contains("throw"))
		{
			//System.out.println("throw caught " + obj.toString() );
			return obj;
		}
		
		boolean b = (Boolean) obj;
		//System.out.println("inner loop bool : " + b);
		if(!b) {
			throw new RuntimeException("assertion failure");
		}
		return null;
	}

	private Object execute(Stmt.Assign stmt, HashMap<String,Object> frame) {
		Expr lhs = stmt.getLhs();
		if(lhs instanceof Expr.Variable) {
			Expr.Variable ev = (Expr.Variable) lhs;
			Object rhs = execute(stmt.getRhs(),frame);
			// We need to perform a deep clone here to ensure the value
			// semantics used in While are preserved.
			frame.put(ev.getName(),deepClone(rhs));
		} else if(lhs instanceof Expr.RecordAccess) {
			Expr.RecordAccess ra = (Expr.RecordAccess) lhs;
			Map<String,Object> src = (Map) execute(ra.getSource(),frame);
			Object rhs = execute(stmt.getRhs(),frame);
			// We need to perform a deep clone here to ensure the value
			// semantics used in While are preserved.
			src.put(ra.getName(), deepClone(rhs));
		} else if(lhs instanceof Expr.IndexOf) {
			Expr.IndexOf io = (Expr.IndexOf) lhs;
			ArrayList<Object> src = (ArrayList) execute(io.getSource(),frame);
			Integer idx = (Integer) execute(io.getIndex(),frame);
			Object rhs = execute(stmt.getRhs(),frame);
			// We need to perform a deep clone here to ensure the value
			// semantics used in While are preserved.
			src.set(idx,deepClone(rhs));
		} else {
			internalFailure("unknown lval encountered (" + lhs + ")", file.filename,stmt);
		}

		return null;
	}

	private Object execute(Stmt.For stmt, HashMap<String,Object> frame) {
		execute(stmt.getDeclaration(),frame);
		while((Boolean) execute(stmt.getCondition(),frame)) {
			Object ret = execute(stmt.getBody(),frame);
			if(ret == BREAK_CONSTANT) {
				break;
			} else if(ret == CONTINUE_CONSTANT) {
				// continue :)
			} else if(ret != null) {
				return ret;
			}
			execute(stmt.getIncrement(),frame);
		}
		return null;
	}
	
	private Object execute(Stmt.ForEach stmt, HashMap<String,Object> frame) {
		if(execute(stmt.getCondition(),frame) instanceof ArrayList)
		{
			ArrayList<Object> arrayList = (ArrayList) execute(stmt.getCondition(),frame);
			execute(stmt.getDeclaration(),frame);
			for(int i=0;i<arrayList.size();i++)
			{
				frame.put(stmt.getDeclaration().getName(), arrayList.get(i));
				//execute(stmt.getBody(),frame);
				Object ret = execute(stmt.getBody(),frame);
				if(ret == BREAK_CONSTANT) {
					break;
				} else if(ret == CONTINUE_CONSTANT) {
					// continue :)
				} else if(ret != null) {
					return ret;
				}
			}
		}
		return null;
	}

	private Object execute(Stmt.Throw stmt, HashMap<String,Object> frame) {
		Expr re = stmt.getCondition();
	    if (re != null) {
	    	Object obj = execute(re, frame) + " throw";
	    	//System.out.println("print obj: " + obj.toString());
	    	return obj;
	    } else {
	      return Collections.EMPTY_SET; // used to indicate a function has returned
	    }  
}
	
	private Object execute(Stmt.Try stmt, HashMap<String,Object> frame) {
		int i=0;
		Object ret = new Object();
		try {
			for(i=0;i<stmt.getBody().size();i++)
			{
				//System.out.println("stmr body : " + stmt.getBody().get(i).toString() );
				ret = execute(stmt.getBody().get(i),frame);
				//System.out.println("print return try: " + ret);
				if(ret != null) {
					if(ret.toString().contains("throw"))
					{
						//System.out.println("thrrrr + " + ret.toString());
						throw null;
					}
					return ret;
				}
		
			}
		}
		catch(Exception e)
		{
			if(e.toString().equals("java.lang.NullPointerException"))
			{
				//System.out.println("Exception is " + e.toString());
				String[] val = ret.toString().split(" ");
				//System.out.println("printing val o " + val[0]);
				return val[0];
			}
			
			Object parameter = new Object();
			//System.out.println("error bodyyy: " + stmt.getBody().get(i).toString());
			if(stmt.getBody().get(i).toString().contains("assert"))
			{
				parameter = deepClone(false);
			}
			List<Stmt.Catch> selectedCatches = new ArrayList<>();
			selectedCatches = stmt.getCatches();
			Object aCatchValue = new Object();
			if(selectedCatches.size()>1)
			{
				if(stmt.getBody().get(i).toString().contains("assert"))
				{
					for(int j=0;j<selectedCatches.size();j++)
					{
						//System.out.println(selectedCatches.get(j).getParameters().get(0).getType());
						if(selectedCatches.get(j).getParameters().get(0).getType().toString().contains("Assert"))
						{
							int code=0;
							if(e.toString().contains("Arithmetic"))
							{
								code = 0;
								parameter = code;
							}
							else if(e.toString().contains("Index"))
							{
								code = 1;
								parameter = code;
							}
							else if(e.toString().contains("Internal") || e.toString().contains("Negative"))
							{
								code = 2;
								parameter = code;
							}
							else if(e.toString().contains("Assert"))
							{
								code = 3;
								parameter = code;
							}
							aCatchValue = execute(selectedCatches.get(j), parameter, frame);
						}
					}
				}
			}
			else
			{
				//System.out.println("catch size : " + selectedCatches.size() + " " + stmt.getCatches().get(0).toString());
				//System.out.println("catch size : " + selectedCatches.size() + " " + stmt.getCatches().get(0).getParameters().get(0).getType());
				//System.out.println("catch size : " + selectedCatches.size() + " " + stmt.getCatches().get(0).getParameters().get(0).name());
				//System.out.println("catch size : " + selectedCatches.size() + " " + e.toString());
				int code=0;
				if(e.toString().contains("Arithmetic"))
				{
					code = 0;
					parameter = code;
				}
				else if(e.toString().contains("Index"))
				{
					code = 1;
					parameter = code;
				}
				else if(e.toString().contains("Internal") || e.toString().contains("Negative"))
				{
					code = 2;
					parameter = code;
				}
				else if(e.toString().contains("Assert"))
				{
					code = 3;
					parameter = code;
				}
				
				aCatchValue = execute(selectedCatches.get(0), parameter, frame);
			}
		}

		return null;
	}
	


	private Object execute(Stmt.Catch stmt, Object paramValue, HashMap<String,Object> frame) {
	//	System.out.println("param value passing correct " + paramValue.toString());
		//System.out.println("para value: " + stmt.getParameters().get(0).getName());
		
	    HashMap<String, Object> frame1 = new HashMap<String, Object>();

	      WhileFile.Parameter parameter = stmt.getParameters().get(0);
	      //System.out.println("parameter: " + parameter.getName()+".code");
	      frame1.put(parameter.getName(), paramValue);
	      frame1.put(parameter.getName()+".code", paramValue);
		
		Object ret = execute(stmt.getBody(),frame1);
		if(ret != null) {
			return ret;
		} 
	return null;
}

	private Object execute(Stmt.While stmt, HashMap<String,Object> frame) {
		while((Boolean) execute(stmt.getCondition(),frame)) {
			Object ret = execute(stmt.getBody(),frame);
			if(ret == BREAK_CONSTANT) {
				break;
			} else if(ret == CONTINUE_CONSTANT) {
				// continue :)
			} else if(ret != null) {
				return ret;
			}
		}
		return null;
	}

	private Object execute(Stmt.IfElse stmt, HashMap<String,Object> frame) {
		boolean condition = (Boolean) execute(stmt.getCondition(),frame);
		if(condition) {
			return execute(stmt.getTrueBranch(),frame);
		} else {
			return execute(stmt.getFalseBranch(),frame);
		}
	}

	private Object execute(Stmt.Break stmt, HashMap<String, Object> frame) {
		return BREAK_CONSTANT;
	}

	private Object execute(Stmt.Continue stmt, HashMap<String, Object> frame) {
		return CONTINUE_CONSTANT;
	}

	private Object execute(Stmt.Switch stmt, HashMap<String, Object> frame) {
		boolean fallThru = false;
		Object value = execute(stmt.getExpr(), frame);
		for (Stmt.Case c : stmt.getCases()) {
			Expr e = c.getValue();
			if (fallThru || e == null || value.equals(execute(e, frame))) {
				Object ret = execute(c.getBody(), frame);
				if(ret == BREAK_CONSTANT) {
					break;
				} else if(ret != null) {
					return ret;
				}
				fallThru = true;
			}
		}
		return null;
	}

	private Object execute(Stmt.Return stmt, HashMap<String,Object> frame) {
		Expr re = stmt.getExpr();
		if(re != null) {
			return execute(re,frame);
		} else {
			return Collections.EMPTY_SET; // used to indicate a function has returned
		}
	}

	private Object execute(Stmt.VariableDeclaration stmt,
			HashMap<String, Object> frame) {
		Expr re = stmt.getExpr();
		Object value;
		if (re != null) {
			value = execute(re, frame);
		} else {
			value = Collections.EMPTY_SET; // used to indicate a variable has
											// been declared
		}
		// We need to perform a deep clone here to ensure the value
		// semantics used in While are preserved.
		frame.put(stmt.getName(), deepClone(value));
		return null;
	}

	/**
	 * Execute a given expression in a given stack frame.
	 *
	 * @param expr
	 *            Expression to execute.
	 * @param frame
	 *            Stack frame mapping variables to their current value.
	 * @return
	 */
	private Object execute(Expr expr, HashMap<String,Object> frame) {
		if(expr instanceof Expr.Binary) {
			//System.out.println("1");
			return execute((Expr.Binary) expr,frame);
		} else if(expr instanceof Expr.Literal) {
			//System.out.println("2");
			return execute((Expr.Literal) expr,frame);
		} else if(expr instanceof Expr.Invoke) {
			//System.out.println("3");
			return execute((Expr.Invoke) expr,frame);
		} else if(expr instanceof Expr.IndexOf) {
			//System.out.println("4");
			return execute((Expr.IndexOf) expr,frame);
		} else if(expr instanceof Expr.ArrayGenerator) {
			//System.out.println("5");
			return execute((Expr.ArrayGenerator) expr,frame);
		} else if(expr instanceof Expr.ArrayInitialiser) {
			//System.out.println("6");
			return execute((Expr.ArrayInitialiser) expr,frame);
		} else if(expr instanceof Expr.RecordAccess) {
			//System.out.println("7");
			return execute((Expr.RecordAccess) expr,frame);
		} else if(expr instanceof Expr.RecordConstructor) {
			//System.out.println("8");
			return execute((Expr.RecordConstructor) expr,frame);
		} else if(expr instanceof Expr.Unary) {
			//System.out.println("9");
			return execute((Expr.Unary) expr,frame);
		} else if(expr instanceof Expr.Variable) {
			//System.out.println("10");
			//boolean b = (Boolean) execute((Expr.Variable) expr,frame);
			//System.out.println("I came here" + b);
			return execute((Expr.Variable) expr,frame);
		} else {
			internalFailure("unknown expression encountered (" + expr + ")", file.filename,expr);
			return null;
		}
	}

	private Object execute(Expr.Binary expr, HashMap<String,Object> frame) {
		// First, deal with the short-circuiting operators first
		//System.out.println("lhs: " + expr.getLhs());
		//System.out.println("rhs: " + expr.getRhs());
		Object lhs = execute(expr.getLhs(), frame);
		
		if(lhs.toString().contains("throw"))
		{
			return lhs;
		}
		switch (expr.getOp()) {
		case AND:
			return ((Boolean)lhs) && ((Boolean)execute(expr.getRhs(), frame));
		case OR:
			return ((Boolean)lhs) || ((Boolean)execute(expr.getRhs(), frame));
		}

		// Second, deal the rest.
		Object rhs = execute(expr.getRhs(), frame);

		if( (lhs.toString().contains("[") && !(rhs.toString().contains("["))) || (!(lhs.toString().contains("[")) && rhs.toString().contains("["))  )
		{
			lhs = lhs.toString().replaceAll("\\[", "").replaceAll("\\]","");
			rhs = rhs.toString().replaceAll("\\[", "").replaceAll("\\]","");
		}
		if( (lhs.toString().contains("{") && !(rhs.toString().contains("{"))) || (!(lhs.toString().contains("{")) && rhs.toString().contains("{"))  )
		{
			lhs = lhs.toString().replaceAll("\\{", "").replaceAll("\\}","");
			rhs = rhs.toString().replaceAll("\\{", "").replaceAll("\\}","");
			lhs = lhs.toString().replaceAll("\\:", "");
			rhs = rhs.toString().replaceAll("\\:", "");
			String[] lhsSplit = lhs.toString().split("=");
			String[] rhsSplit = rhs.toString().split("=");
			if(lhsSplit.length>1)
			{
				lhs = lhsSplit[1];
			}
			if(rhsSplit.length>1)
			{
				rhs = rhsSplit[1];
			}
		}
		if( (lhs.toString().equals("true") || lhs.toString().equals("false")) && rhs.toString().matches("-?\\d+(\\.\\d+)?"))
		{
			if(lhs.toString().equals("true"))
			{
				lhs = 0;
			}
			else
			{
				lhs = 1;
			}
		}
		if( (rhs.toString().equals("true") || rhs.toString().equals("false")) && lhs.toString().matches("-?\\d+(\\.\\d+)?"))
		{
			if(rhs.toString().equals("true"))
			{
				rhs = 0;
			}
			else
			{
				rhs = 1;
			}
		}
		//System.out.println("lhs af " + lhs.toString());
		//System.out.println("lhs af " + rhs.toString());
		
		switch (expr.getOp()) {
		case ADD:
			if (lhs instanceof Integer && rhs instanceof Integer) {
                return ((Integer) lhs) + ((Integer) rhs);
            } else if(lhs instanceof Integer && rhs instanceof Double){
                return Double.parseDouble(Integer.toString((Integer) lhs)) + ((Double) rhs);
            } else if(lhs instanceof Double && rhs instanceof Integer){
                return ((Double) lhs) + Double.parseDouble(Integer.toString((Integer) rhs));
            } else if(lhs instanceof Double && rhs instanceof Double){
                return ((Double) lhs) + ((Double) rhs);
            }
		case SUB:
			if (lhs instanceof Integer && rhs instanceof Integer) {
                return ((Integer) lhs) - ((Integer) rhs);
            } else if(lhs instanceof Integer && rhs instanceof Double){
                return Double.parseDouble(Integer.toString((Integer) lhs)) - ((Double) rhs);
            } else if(lhs instanceof Double && rhs instanceof Integer){
                return ((Double) lhs) - Double.parseDouble(Integer.toString((Integer) rhs));
            } else if(lhs instanceof Double && rhs instanceof Double){
                return ((Double) lhs) - ((Double) rhs);
            }
		case MUL:
			if (lhs instanceof Integer && rhs instanceof Integer) {
                return ((Integer) lhs) * ((Integer) rhs);
            } else if(lhs instanceof Integer && rhs instanceof Double){
                return Double.parseDouble(Integer.toString((Integer) lhs)) * ((Double) rhs);
            } else if(lhs instanceof Double && rhs instanceof Integer){
                return ((Double) lhs) * Double.parseDouble(Integer.toString((Integer) rhs));
            } else  if(lhs instanceof Double && rhs instanceof Double) {
                return ((Double) lhs) * ((Double) rhs);
            }
		case DIV:
			if (lhs instanceof Integer && rhs instanceof Integer) {
                return ((Integer) lhs) / ((Integer) rhs);
            } else if(lhs instanceof Integer && rhs instanceof Double){
                return Double.parseDouble(Integer.toString((Integer) lhs)) / ((Double) rhs);
            } else if(lhs instanceof Double && rhs instanceof Integer){
                return ((Double) lhs) / Double.parseDouble(Integer.toString((Integer) rhs));
            } else if(lhs instanceof Double && rhs instanceof Double){
                return ((Double) lhs) / ((Double) rhs);
            }
		case REM:
			if (lhs instanceof Integer && rhs instanceof Integer) {
                return ((Integer) lhs) % ((Integer) rhs);
            } else if(lhs instanceof Integer && rhs instanceof Double){
                return Double.parseDouble(Integer.toString((Integer) lhs)) % ((Double) rhs);
            } else if(lhs instanceof Double && rhs instanceof Integer){
                return ((Double) lhs) % Double.parseDouble(Integer.toString((Integer) rhs));
            } else if(lhs instanceof Double && rhs instanceof Double){
                return ((Double) lhs) % ((Double) rhs);
            }
		case EQ:
			//System.out.println("I vame here");
			//System.out.println(lhs.toString() + " " + rhs.toString());
			if(lhs instanceof Integer && rhs instanceof Integer)
			{
				return lhs.equals(rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Double)
			{
				return lhs.equals(rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Integer)
			{
				return lhs.equals(Double.parseDouble(Integer.toString((Integer) rhs)));
			}
			else if(lhs instanceof Integer && rhs instanceof Double)
			{
				return rhs.equals(Double.parseDouble(Integer.toString((Integer) lhs)));
			}
			else if(lhs instanceof ArrayList && rhs instanceof ArrayList)
			{
				boolean b = (boolean) processArrayList(lhs,rhs);
				return b;
			}
			else if(lhs instanceof HashMap && rhs instanceof HashMap)
			{
				boolean b = (boolean) processHashMap(lhs,rhs);
				return b;
			}
			else if(lhs instanceof Boolean && rhs instanceof Boolean)
			{
				boolean b1 = (boolean) lhs;
				boolean b2 = (boolean) rhs;
				
				//System.out.println("Boolean");
				return b1==b2;
			}
			else if(lhs.toString().matches("-?\\d+(\\.\\d+)?") && rhs.toString().matches("-?\\d+(\\.\\d+)?"))
			{
				int l = Integer.parseInt(lhs.toString());
				int r = Integer.parseInt(rhs.toString());
				return l==r;
			}
			else if(lhs.toString().equals("true") || lhs.toString().equals("false") || rhs.toString().equals("true") || rhs.toString().equals("false"))
			{
				boolean b1 = (boolean) lhs;
				boolean b2 = (boolean) rhs;
				return b1==b2;
			}
		case NEQ:
			if(lhs instanceof Integer && rhs instanceof Integer)
			{
				return !lhs.equals(rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Double)
			{
				return !lhs.equals(rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Integer)
			{
				return !lhs.equals(Double.parseDouble(Integer.toString((Integer) rhs)));
			}
			else if(lhs instanceof Integer && rhs instanceof Double)
			{
				return !rhs.equals(Double.parseDouble(Integer.toString((Integer) lhs)));
			}
			else if(lhs instanceof ArrayList && rhs instanceof ArrayList)
			{
				boolean b = (boolean) processArrayList(lhs,rhs);
				return !b;
			}
			else if(lhs instanceof HashMap && rhs instanceof HashMap)
			{
				boolean b = (boolean) processHashMap(lhs,rhs);
				return !b;
			}
			else if(lhs.toString().matches("-?\\d+(\\.\\d+)?") && rhs.toString().matches("-?\\d+(\\.\\d+)?"))
			{
				int l = Integer.parseInt(lhs.toString());
				int r = Integer.parseInt(rhs.toString());
				return !(l==r);
			}
			else if(lhs.toString().equals("true") || lhs.toString().equals("false") || rhs.toString().equals("true") || rhs.toString().equals("false"))
			{
				boolean b1 = (boolean) lhs;
				boolean b2 = (boolean) rhs;
				return b1==b2;
			}
		case LT:
			if(lhs instanceof Integer && rhs instanceof Integer)
			{
				return ((Integer) lhs) < ((Integer) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Double)
			{
				return ((Double) lhs) < ((Double) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Integer)
			{
				return ((Double) lhs) < Double.parseDouble(Integer.toString((Integer) rhs));
			}
			else if(lhs instanceof Integer && rhs instanceof Double)
			{
				return Double.parseDouble(Integer.toString((Integer) lhs)) < ((Double) rhs);
			}
		case LTEQ:
			if(lhs instanceof Integer && rhs instanceof Integer)
			{
				return ((Integer) lhs) <= ((Integer) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Double)
			{
				return ((Double) lhs) <= ((Double) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Integer)
			{
				return ((Double) lhs) <= Double.parseDouble(Integer.toString((Integer) rhs));
			}
			else if(lhs instanceof Integer && rhs instanceof Double)
			{
				return Double.parseDouble(Integer.toString((Integer) lhs)) <= ((Double) rhs);
			}
		case GT:
			if(lhs instanceof Integer && rhs instanceof Integer)
			{
				return ((Integer) lhs) > ((Integer) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Double)
			{
				return ((Double) lhs) > ((Double) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Integer)
			{
				return ((Double) lhs) > Double.parseDouble(Integer.toString((Integer) rhs));
			}
			else if(lhs instanceof Integer && rhs instanceof Double)
			{
				return Double.parseDouble(Integer.toString((Integer) lhs)) > ((Double) rhs);
			}
		case GTEQ:
			if(lhs instanceof Integer && rhs instanceof Integer)
			{
				return ((Integer) lhs) >= ((Integer) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Double)
			{
				return ((Double) lhs) >= ((Double) rhs);
			}
			else if(lhs instanceof Double && rhs instanceof Integer)
			{
				return ((Double) lhs) >= Double.parseDouble(Integer.toString((Integer) rhs));
			}
			else if(lhs instanceof Integer && rhs instanceof Double)
			{
				return Double.parseDouble(Integer.toString((Integer) lhs)) >= ((Double) rhs);
			}
		}

		internalFailure("unknown binary expression encountered (" + expr + ")",
				file.filename, expr);
		return null;
	}

	private Object processArrayList(Object lhs,Object rhs) {	
		ArrayList<Object> lhsObj = (ArrayList) lhs;
		ArrayList<Object> rhsObj = (ArrayList) rhs;
		int i=0;
		int flag=0;
		if(lhsObj.size() == rhsObj.size())
		{
		 for (Object lObj : lhsObj)
		 {
			 if((lObj instanceof Integer && rhsObj.get(i) instanceof Integer))
				{
				 if(!lObj.equals(rhsObj.get(i)))
					flag=1;
				}
				else if((lObj instanceof Double && rhsObj.get(i) instanceof Double))
				{
					if(!lObj.equals(rhsObj.get(i)))
					flag=1;
				}
				else if((lObj instanceof Double && rhsObj.get(i) instanceof Integer))
				{
					if(!rhsObj.get(i).equals(Double.parseDouble(Integer.toString((Integer) lObj))))
					flag=1;
				}
				else if((lObj instanceof Integer && rhsObj.get(i) instanceof Double))
				{
					if(!rhsObj.get(i).equals(Double.parseDouble(Integer.toString((Integer) lObj))))
					flag=1;
				}
			 i++;
		 }
		}
		 if(flag==0)
		 {
			 return true;
		 }
		 else
		 {
			 return false;
		 }
		
	}
	private Object processHashMap(Object lhs,Object rhs) {
		HashMap<Object, Object> lObj =  (HashMap) lhs;
		HashMap<Object, Object> rObj =  (HashMap) rhs;
		HashMap<Object, Double> newLhs =  new HashMap<>();
		HashMap<Object, Double> newRhs =  new HashMap<>();
		
		Iterator it1 = lObj.entrySet().iterator();
		Iterator it2 = lObj.entrySet().iterator();
	    while (it1.hasNext()) {
	        Map.Entry pair = (Map.Entry)it1.next();
	        if(pair.getValue() instanceof Integer)
	        {
	        	Double d = Double.parseDouble(Integer.toString((Integer) pair.getValue()));
	        	newLhs.put(pair.getKey(), d);
	        }   
	    }
	    while (it2.hasNext()) {
	        Map.Entry pair = (Map.Entry)it2.next();
	        if(pair.getValue() instanceof Integer)
	        {
	        	Double d = Double.parseDouble(Integer.toString((Integer) pair.getValue()));
	        	newRhs.put(pair.getKey(), d);
	        }
	        
	    }
	    if(newLhs.equals(newRhs))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
		
	}
	
	private Object execute(Expr.Literal expr, HashMap<String,Object> frame) {
		Object o = expr.getValue();
		// Check whether any coercions required
		boolean b1 = o instanceof Character;
		boolean b2 = o instanceof String;
		if(o instanceof Character) {
			char c = ((Character)o);
			return c;
		} else if(o instanceof String) {
			String s = ((String)o);
			ArrayList<Integer> list = new ArrayList<>();
			for(int i=0;i!=s.length();++i) {
				list.add((int) s.charAt(i));
			}
			return list;
		}
		// Done
		return o;
	}

	private Object execute(Expr.Invoke expr, HashMap<String, Object> frame) {
		List<Expr> arguments = expr.getArguments();
		Object[] values = new Object[arguments.size()];
		for (int i = 0; i != values.length; ++i) {
			// We need to perform a deep clone here to ensure the value
			// semantics used in While are preserved.
			values[i] = deepClone(execute(arguments.get(i), frame));
		}
		WhileFile.MethodDecl fun = (WhileFile.MethodDecl) declarations.get(expr
				.getName());
		Object obj = execute(fun, values);
		//System.out.println("method return throw : " + obj.toString());
		return obj;
	}

	private Object execute(Expr.IndexOf expr, HashMap<String,Object> frame) {
		Object _src = execute(expr.getSource(),frame);
		int idx = (Integer) execute(expr.getIndex(),frame);
		if(_src instanceof String) {
			String src = (String) _src;
			return src.charAt(idx);
		} else {
			ArrayList<Object> src = (ArrayList<Object>) _src;
			return src.get(idx);
		}
	}

	private Object execute(Expr.ArrayGenerator expr, HashMap<String, Object> frame) {
		Object value = execute(expr.getValue(),frame);
		int size = (Integer) execute(expr.getSize(),frame);
		ArrayList<Object> ls = new ArrayList<Object>();
		for (int i = 0; i < size; ++i) {
			ls.add(value);
		}
		return ls;
	}

	private Object execute(Expr.ArrayInitialiser expr,
			HashMap<String, Object> frame) {
		List<Expr> es = expr.getArguments();
		ArrayList<Object> ls = new ArrayList<Object>();
		for (int i = 0; i != es.size(); ++i) {
			ls.add(execute(es.get(i), frame));
		}
		return ls;
	}

	private Object execute(Expr.RecordAccess expr, HashMap<String, Object> frame) {
		//System.out.println("execute(expr.getSource(), frame): " + execute(expr.getSource(), frame) );
		Object obj = execute(expr.getSource(), frame);
		if(obj instanceof Integer)
		{
			return obj;
		}
		if(obj instanceof Boolean)
		{
			return 3;
		}
		
		HashMap<String, Object> src = (HashMap) obj;
		return src.get(expr.getName());
	}

	private Object execute(Expr.RecordConstructor expr, HashMap<String,Object> frame) {
		List<Pair<String,Expr>> es = expr.getFields();
		HashMap<String,Object> rs = new HashMap<String,Object>();

		for(Pair<String,Expr> e : es) {
			rs.put(e.first(),execute(e.second(),frame));
		}

		return rs;
	}

	private Object execute(Expr.Unary expr, HashMap<String, Object> frame) {
		Object value = execute(expr.getExpr(), frame);
		switch (expr.getOp()) {
		case NOT:
			return !((Boolean) value);
		case NEG:
            if (value instanceof Integer) {
                return -((Integer) value);
            } else if (value instanceof Double){
                return -((Double) value);
            }
		case LENGTHOF:
			return ((ArrayList) value).size();
		}

		internalFailure("unknown unary expression encountered (" + expr + ")",
				file.filename, expr);
		return null;
	}

	private Object execute(Expr.Variable expr, HashMap<String,Object> frame) {
		return frame.get(expr.getName());
	}

	/**
	 * Perform a deep clone of the given object value. This is either a
	 * <code>Boolean</code>, <code>Integer</code>, , <code>Character</code>,
	 * <code>String</code>, <code>ArrayList</code> (for lists) or
	 * <code>HaspMap</code> (for records). Only the latter two need to be
	 * cloned, since the others are immutable.
	 *
	 * @param o
	 * @return
	 */
	private Object deepClone(Object o) {
		if (o instanceof ArrayList) {
			ArrayList<Object> l = (ArrayList) o;
			ArrayList<Object> n = new ArrayList<Object>();
			for (int i = 0; i != l.size(); ++i) {
				n.add(deepClone(l.get(i)));
			}
			return n;
		} else if (o instanceof HashMap) {
			HashMap<String, Object> m = (HashMap) o;
			HashMap<String, Object> n = new HashMap<String, Object>();
			for (String field : m.keySet()) {
				n.put(field, deepClone(m.get(field)));
			}
			return n;
		} else {
			// other cases can be ignored
			return o;
		}
	}

	/**
	 * Convert the given object value to a string. This is either a
	 * <code>Boolean</code>, <code>Integer</code>, <code>Character</code>,
	 * <code>String</code>, <code>ArrayList</code> (for lists) or
	 * <code>HaspMap</code> (for records). The latter two must be treated
	 * recursively.
	 *
	 * @param o
	 * @return
	 */
	private String toString(Object o) {
		if (o instanceof ArrayList) {
			ArrayList<Object> l = (ArrayList) o;
			String r = "[";
			for (int i = 0; i != l.size(); ++i) {
				if(i != 0) {
					r = r + ", ";
				}
				r += toString(l.get(i));
			}
			return r + "]";
		} else if (o instanceof HashMap) {
			HashMap<String, Object> m = (HashMap) o;
			String r = "{";
			boolean firstTime = true;
			ArrayList<String> fields = new ArrayList<String>(m.keySet());
			Collections.sort(fields);
			for (String field : fields) {
				if(!firstTime) {
					r += ",";
				}
				firstTime=false;
				r += field + ":" + toString(m.get(field));
			}
			return r + "}";
		} else if(o != null) {
			// other cases can use their default toString methods.
			return o.toString();
		} else {
			return "null";
		}
	}

	private Object BREAK_CONSTANT = new Object() {};
	private Object CONTINUE_CONSTANT = new Object() {};
}
